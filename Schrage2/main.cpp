#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <queue>
#include <functional>

struct Task{
    int r; // termin dostepnosci
    int p; // czas obslugi zadania
    int q; // czas dostarczenia uslugi
};

bool isQSmaller(const Task& task1, const Task& task2) {
    return task1.q < task2.q;
}

bool isRGreater(const Task &task1, const Task &task2) {
    return task1.r > task2.r;
}

void printTasks(std::vector<Task> tasks) {
      for(const auto& task: tasks){
        std::cout << task.r << " " << task.p << " " << task.q << std::endl;
    }
}

int cmaxSchrage(std::vector<Task> tasks) {
    int t = 0;
    int cmax = 0;
    Task e;
    Task h;
    std::vector<Task> results;
    // zbior zadan gotowych do realizacji
    std::priority_queue <Task, std::vector<Task>, std::function<bool(Task, Task)> > g(isQSmaller);
    // zbior zadan nieuszeregowanych
    std::priority_queue < Task, std::vector<Task>, std::function<bool(Task, Task)> > n(isRGreater);
    Task l;
    l.r = 0; l.p = 0; l.q = 10000000;

    for(const auto& task: tasks){
        n.push(task);
    }

    while(!g.empty() || !n.empty()) {
        while(!n.empty() && n.top().r <= t) {
            e = n.top();
            g.push(e);
            n.pop();

            if(e.q > l.q) {
                l.p = t - e.r;
                t = e.r;

                if(l.p > 0) {
                    g.push(l);
                }
            }
        }

        if(g.empty()) {
            t = n.top().r;
        }
        else {
            e = g.top();
            g.pop();
            l = e;
            t += e.p;
            cmax = std::max(cmax, t+e.q);
        }
    }
    return cmax;
}

int main(int argc, char* argv[]) {
    std::vector<Task> tasks;
    int numberOfTasks = 0;

    std::ifstream inputFile;
    std::string fileName(argv[1]);
    inputFile.open(fileName);

    if(inputFile.is_open()){
        inputFile >> numberOfTasks;
        for (int i = 0; i < numberOfTasks; ++i) {
            Task task;
            inputFile >> task.r >> task.p >> task.q;
            tasks.push_back(task);
        }
        inputFile.close();
    }

    std::cout << cmaxSchrage(tasks) << std::endl;

    return 0;
}
