#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <queue>

std::vector<int> countLP(std::vector<int>,std::vector<int>);

int main(int argc, char* argv[]) {

    int numberOfTasks = 0;
    int numberOfMachines = 0;

    std::vector<int> T,M,P;
    T.push_back(0);
    M.push_back(0);
    P.push_back(0);

    std::vector<std::vector<int>> perm;
    int temp;
    std::string fileName(argv[1]);
    std::ifstream inputFile;

    inputFile.open(fileName);
    if(inputFile.is_open()){
        inputFile >> numberOfTasks;

        for (int i = 0; i < numberOfTasks; ++i)	{
            inputFile >> temp;
            T.push_back(temp);
        }

        for (int i = 0; i < numberOfTasks; ++i)	{
            inputFile >> temp;
            M.push_back(temp);
        }

        for (int i = 0; i < numberOfTasks; ++i)	{
            inputFile >> temp;
            P.push_back(temp);
        }

        inputFile >> numberOfMachines;

        perm.resize(numberOfMachines);

        for (int i = 0; i < numberOfMachines; ++i)	{
            for (int j = 0; j < numberOfMachines; ++j)	{
                inputFile >> temp;
                perm[i].push_back(temp);
            }
            inputFile >> temp;
        }
        inputFile.close();
    }

    std::vector<int> PT(numberOfTasks+1);
    std::vector<int> PM(numberOfTasks+1);
    std::fill(PM.begin(), PM.end(), 0);
    std::fill(PT.begin(), PT.end(), 0);

    for(int i = 1; i <= numberOfTasks; ++i) {
        PT[T[i]] = i;
        PM[M[i]] = i;
    }

    std::vector<int> LP = countLP(T,M);
    std::vector<int> C (numberOfTasks+1);
    std::vector<int> S (numberOfTasks+1);
    std::queue<int> kolejka;
    std::fill(C.begin(), C.end(), 0);

    // na poczatek kolejki dodaje elementy o lp = 0
    for(int i = 1; i <= numberOfTasks; ++i) {
        if(LP[i] == 0) {
            kolejka.push(i);
        }
    }


    while(!kolejka.empty()) {
        int elem = kolejka.front();

        C[elem] = std::max(C[PT[elem]],C[PM[elem]]) + P[elem];
        S[elem] = C[elem] - P[elem];

        --LP[T[elem]];
        --LP[M[elem]];

        if(LP[M[elem]] == 0 ) {
            kolejka.push(M[elem]);
        }
        if(LP[T[elem]] == 0) {
            kolejka.push(T[elem]);
        }

        kolejka.pop();

    }
    int cmax = 0;
    for(int i = 1; i < numberOfTasks + 1 ; ++i) {
        if (C[i] > cmax) cmax = C[i];
        std::cout << S[i] << " " << C[i] << std::endl;
    }
    std::cout << "CMAX: " << cmax << std::endl;
    return 0;
}

std::vector<int> countLP(std::vector<int> T,std::vector<int> M) {
    int size = T.size();
    std::vector<int> result(size);
    std::fill(result.begin(),result.end(),0);

    for(int i = 0; i < size; ++i) {
        ++result[M[i]];
        ++result[T[i]];
    }

    return result;


}


