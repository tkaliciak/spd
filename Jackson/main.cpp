#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

struct Task{
    int r; // termin dostepnosci
    int p; // czas obslugi zadania
    bool operator<(const Task &rhs) const { return r < rhs.r; }
};

int main(int argc, char* argv[]) {
    
    std::vector<Task> tasks;
    std::vector<int> cTab;
    int numberOfTasks = 0;


    std::string fileName(argv[1]);
    std::ifstream inputFile;

    inputFile.open(fileName);
    if(inputFile.is_open()){
        inputFile >> numberOfTasks;
        for (unsigned i = 0; i < numberOfTasks; ++i)	{
            Task task;
            inputFile >> task.r >> task.p;
            tasks.push_back(task);
        }
        inputFile.close();
    }
    std::sort(tasks.begin(), tasks.end());
    
    cTab.push_back(tasks[0].r + tasks[0].p);

    for(int i = 1; i <= numberOfTasks; ++i) {
        cTab[i] = (std::max(cTab[i-1], tasks[i].r) + tasks[i].p);
    }
    std::cout << cTab[tasks.size()] << " ";

    return 0;
}