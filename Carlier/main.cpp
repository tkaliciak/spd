#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <queue>
#include <functional>

struct Task{
    uint id;
    int r; // termin dostepnosci
    int p; // czas obslugi zadania
    int q; // czas dostarczenia uslugi
};

bool isQSmaller(const Task& task1, const Task& task2) {
    return task1.q < task2.q;
}

bool isRGreater(const Task &task1, const Task &task2) {
    return task1.r > task2.r;
}

void printTasks(std::vector<Task> tasks) {
      for(const auto& task: tasks){
        std::cout << task.r << " " << task.p << " " << task.q << std::endl;
    }
}

int wyznaczABC(std::vector<Task> tasks, std::vector<uint> pi, int dmax) {
    int a(0),b(0),c(0);

    for(uint i = 1; i < tasks.size(); i++) {
        if(cmax > )
    }
    return 0;
}

int cmaxSchrage(std::vector<Task> tasks) {
    int t = 0;
    int cmax = 0;
    int k = 0;
    Task e;
    Task h;
    std::vector<uint> results;
    std::vector<Task> results;

    // zbior zadan gotowych do realizacji
    std::priority_queue <Task, std::vector<Task>, std::function<bool(Task, Task)> > g(isQSmaller);
    // zbior zadan nieuszeregowanych
    std::priority_queue <Task, std::vector<Task>, std::function<bool(Task, Task)> > n(isRGreater);


    for(const auto& task: tasks){
        n.push(task);
    }

    while(!g.empty() || !n.empty()) {
        while(!n.empty() && n.top().r <= t) {
            e = n.top();
            g.push(e);
            n.pop();
        }
        if(g.empty()) {
            t = n.top().r;
        }
        else {
            // termin rozpoczecia
           // h.r = t;
            e = g.top();
            g.pop();
            // termin zakonczeniap
           // h.p = h.r + e.p;
            t += e.p;
            if(cmax <= t + e.q) {
                cmax = t + e.q;
                b = k;
            }
            // termin dostarczenia
           // h.q = t+e.q;
           // h.id = e.id;
            ++k;
            results.push_back(e.id);
        }
    }
    std::cout << b << "    d" << std::endl;
    wyznaczABC(tasks, results, cmax);
    return cmax;
}



int main(int argc, char* argv[]) {
    std::vector<Task> tasks;
    int numberOfTasks = 0;

    std::ifstream inputFile;
    std::string fileName(argv[1]);
    inputFile.open(fileName);

    if(inputFile.is_open()){
        inputFile >> numberOfTasks;
        for (uint i = 0; i < numberOfTasks; ++i) {
            Task task;
            task.id = i;
            inputFile >> task.r >> task.p >> task.q;
            tasks.push_back(task);
        }
        inputFile.close();
    }

    std::cout << cmaxSchrage(tasks) << std::endl;

    return 0;
}
